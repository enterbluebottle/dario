# Dario

*Dario* is a LaTeX package designed to be used for projects that mix prose and play formatting.
It provides a simple syntax for formatting playscripts, along with a few quality of life commands and formatting changes that I find useful for my projects.

The playscript formatting is based on my copy of Dario Fo's *Accidental Death of an Anarchist*.

The idea for using the enumitem package's 'description' environment came from [this blog post](http://kevindonnelly.org.uk/2011/10/latex-template-for-playscripts/).

## Use

Copy `dario.sty` into the search path of LaTeX.

To build to docs, run `pdflatex dario.dtx`.
